import React, {useEffect, useState} from 'react';
import { Style } from "react-style-tag";
import JsxParser from "react-jsx-parser";
import Timer from "react-compound-timer";
import styled from 'styled-components';

const Prompt = styled.div`
    font-size: 1.5em
`;
const LiveTemplate = styled.div`
    margin: 20px;
    padding: 20px;
    border: ${props => !props.jsxString ? 'dashed black' : 'none'};
    };
`;

/**
 * Display a react com
 * @param jsxString
 * @param cssString
 * @constructor
 */
const TemplateDisplay = ({jsxString, cssString}) => {
  
    return (
        <div>
  <Style>{cssString}</Style>
            <JsxParser components={{Timer}} jsx={jsxString} bindings={
                {
                    // This is called from the formatValue attribute of the Timer coming in htmlString
                    formatValue: (value) => `${(value < 10 ? `0${value}` : value)}`
                }
            }/>
        </div>
          
    );
}

export default TemplateDisplay;