import React from "react";
import ReactDOM from "react-dom";
import Timer from "react-compound-timer";
import { timerRef } from "./consts";

export default function (editor, opt = {}) {
  const c = opt;
  const domc = editor.DomComponents;
  const defaultType = domc.getType("default");
  const defaultModel = defaultType.model;
  const defaultView = defaultType.view;
  const pfx = c.timerClsPfx;

  domc.addType(timerRef, {
    model: defaultModel.extend(
      {
        defaults: {
          ...defaultModel.prototype.defaults,
          startFrom: c.startTime,
          timerLabel: c.timerLabel,
          displayLabels: c.displayLabels,
          labels: {
            labelDays: c.labelDays,
            labelHours: c.labelHours,
            labelMinutes: c.labelMinutes,
            labelSeconds: c.labelSeconds,
          },
          droppable: false,
          traits: [
            {
              label: "Start",
              name: "startFrom",
              changeProp: 1,
              type: "datetime-local", // can be 'date'
            },
            {
              label: "Label",
              name: "timerLabel",
              changeProp: 1,
            },
            {
              label: "Display labels",
              name: "displayLabels",
              type: "checkbox",
              changeProp: 1,
            },
          ],
        },
      },
      {
        isComponent(el) {
          //console.log('isComponent', el);
          //debugger;
          if (
            (el.getAttribute && el.getAttribute("data-gjs-type") == timerRef) ||
            (el.attributes && el.attributes["data-gjs-type"] == timerRef)
          ) {
            return {
              type: timerRef,
            };
          }
        },
      }
    ),

    view: defaultView.extend({
      // Listen to changes of startFrom, timerLabel or displayLabels managed by the traits
      init() {
        this.listenTo(
          this.model,
          "change:startFrom change:timerLabel change:displayLabels",
          this.handleChanges
        );
      },

      // Called whenever startFrom, timerLabel or displayLabels changes
      handleChanges(e) {
        /// Force rerender
        // Make sure we start react from scratch for el
        ReactDOM.unmountComponentAtNode(this.el);
        this.render();
      },

      onRender({ el }) {
        // Calc initialTime. If startFrom is set in the trait, then calculate, otherwise leave it 0
        let initialTime = 0;

        // Initially show timer proceeding forward
        let direction = "forward";

        // If startFrom is set, then set this as the initial time and set direction fo backward
        if (this.model.attributes.startFrom != "") {
          const startFrom = this.model.attributes.startFrom;
          var start = Date.parse(startFrom);
          var now = new Date().getTime();
          initialTime = start - now;
          direction = "backward";
        }

        const comps = this.model.get("components");
        comps.reset();
        comps.add(
          `<span className="timer-label">${this.model.attributes.timerLabel}</span>`
        );
        const compString =
          `<Timer
                            ` +
          (direction == "backward" ? `initialTime="${initialTime}"` : "") +
          `
                            direction="${direction}"
                            formatValue={formatValue}
                        >
                        <span className="timer-days">
                            <Timer.Days/>${
                              this.model.attributes.displayLabels
                                ? " " +
                                  this.model.attributes.labels.labelDays +
                                  " "
                                : ", "
                            }
                        </span>
                        <span className="timer-hours">
                            <Timer.Hours/>${
                              this.model.attributes.displayLabels
                                ? " " +
                                  this.model.attributes.labels.labelHours +
                                  " "
                                : ":"
                            }
                        </span>
                            <span className="timer-minutes">
                            <Timer.Minutes/>${
                              this.model.attributes.displayLabels
                                ? " " +
                                  this.model.attributes.labels.labelMinutes +
                                  " "
                                : ":"
                            }
                        </span>
                            <span className="timer-seconds">
                            <Timer.Seconds/>${
                              this.model.attributes.displayLabels
                                ? " " +
                                  this.model.attributes.labels.labelSeconds
                                : ""
                            }
                        </span>
                        </Timer>`;
        comps.add(compString);
        ReactDOM.render(
          <>
            {this.model.attributes.timerLabel != "" ? (
              <span className="timer-label">
                {this.model.attributes.timerLabel}:{" "}
              </span>
            ) : (
              ""
            )}
            <Timer
              initialTime={initialTime}
              direction={direction}
              formatValue={(value) => `${value < 10 ? `0${value}` : value}`}
            >
              <span className="timer-days">
                <Timer.Days />
                {this.model.attributes.displayLabels
                  ? " " + this.model.attributes.labels.labelDays + " "
                  : ", "}
              </span>
              <span className="timer-hours">
                <Timer.Hours />
                {this.model.attributes.displayLabels
                  ? " " + this.model.attributes.labels.labelHours + " "
                  : ":"}
              </span>
              <span className="timer-minutes">
                <Timer.Minutes />
                {this.model.attributes.displayLabels
                  ? " " + this.model.attributes.labels.labelMinutes + " "
                  : ":"}
              </span>
              <span className="timer-seconds">
                <Timer.Seconds />
                {this.model.attributes.displayLabels
                  ? " " + this.model.attributes.labels.labelSeconds
                  : ""}
              </span>
            </Timer>
          </>,
          el
        );
      },
    }),
  });
}
